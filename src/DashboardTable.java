import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.DecimalFormat;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class DashboardTable {

	private JFrame frame;
	private static JTable thresholdConfigTable;
	private static JTable mainConfigTable;
	private JMenuBar menuBar;
	private JMenu mnNewMenu;
	private JMenuItem mntmNewMenuItem;
	private JMenuItem mntmNewMenuItem_1;

	final JFileChooser fc = new JFileChooser();
	private JSplitPane splitPane;
	private JScrollPane scrollPane_2;
	private JScrollPane scrollPane_3;

	private JPanel panel;
	private JButton btnNewButton;
	private JCheckBox chckbxExportLog;
	private JCheckBox avoidCollisionCheckBox;
	private JSplitPane splitPane_1;
	private JScrollPane scrollPane;
	private JTable table;
	private JTable resultTable;
	private JScrollPane scrollPane_1;
	private JComboBox comboBox;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DashboardTable window = new DashboardTable();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public DashboardTable() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("KanSIM Beta V.2 - Microscopic Traffic Simulator (3-to-2 Freeway Configuration)");
		frame.setBounds(50, 50, 1080, 900);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frame.getContentPane().setLayout(new BorderLayout(0, 0));

		splitPane = new JSplitPane();
		frame.getContentPane().add(splitPane, BorderLayout.NORTH);

		
		scrollPane_2 = new JScrollPane();

		scrollPane_3 = new JScrollPane();
		splitPane.setLeftComponent(scrollPane_3);

		splitPane.setRightComponent(scrollPane_2);
		splitPane.setDividerLocation(580);
		mainConfigTable = new JTable();
		mainConfigTable.setModel(new DefaultTableModel(
				new Object[][] { 
						{ "Input Car rate (Vehicals/Hour)", "1600", "nCar" }, 
						{ "Simulation Time(s)", "1800", "t" },
						{ "v Initial (m/s)", "7", "vInit" }, 
						{ "v Max (m/s)", "33.5", "vMax" },
						{ "Road Length (m)", "4100", "roadLength" }, { "Lane drop at (m)", "3000", "laneDrop" },
						{ "Min safe constant gap MDV (gmin_MDV)", "3", "gapMinMDV" },
						{ "Min safe constant gap CAV (gmin_CAV)", "3", "gapMinCAV" },

						{ "Min distance for free lane changes (gapFree)", "30", "gapFree" },

						{ "CAV: Lane change when close to (m)", "800", "laneChangeCAV" },

						{ "MDV: Lane change when close to (m)", "800", "laneChangeMDV" },
						{ "Car length (m)", "5", "carLength" }, 
						{ "CAV:MDV", "10:90", "cavMdvRatio" },
						// { "CAV: Constant k", "0.41", "kCAV" },
						// { "MDV: Constant k", "0.41", "kMDV" },
						{ "CAV: Relaxation Time [τ]", "2.937", "relaxCAV" },
						{ "MDV: Relaxation Time [τ]", "2.937", "relaxMDV" },
						{ "Critical Space (m)", "100", "sc" }, 
						{ "CAV: sensitivity constant [λ]", "0.895", "cavSensitivity" },
						{ "MDV: sensitivity constant [λ]", "0.895", "mdvSensitivity" },
						{ "Max Deceleration rate (m/s^2)", "-3.4", "minA" },
						{ "Max Acceleration rate (m/s^2)", "2", "maxA" }, 
						{ "Vd (m/s)", "31.08|2.175", "vD" },
						{ "Time to start calculate Average Density", "900", "tAvgDensity" },
						{ "(Before LD) Space to calculate Space Mean Speed (m)", "100", "spaceMeanBeforeLD" },

						{ "(After LD) Space to calculate Space Mean Speed (m)", "100", "spaceMeanAfterLD" },
						{ "Output Interval (minute)", "5", "outputInterval" },
						
						{ "MDV Vs V1", "14.3", "mdvVsV1" },
						{ "MDV Vs V2", "15.997", "mdvVsV2" },
						{ "MDV Interaction Length", "15.219", "mdvVsLint" },
						{ "MDV Beta [β MDV]", "1.508", "mdvVsBeta" },
						
						{ "CAV Vs V1", "14.3", "cavVsV1" },
						{ "CAV Vs V2", "6", "cavVsV2" },
						{ "CAV P", "0.905", "cavVsP" },
						{ "CAV Interaction Length", "15.219", "cavVsLint" },
						{ "CAV Beta [β CAV]", "1.508", "cavVsBeta" }
						
						
						},
				new String[] { "Data", "Value", "Var" }) {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			boolean[] columnEditables = new boolean[] { false, true, false };

			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		mainConfigTable.getColumnModel().getColumn(1).setPreferredWidth(50);

		mainConfigTable.removeColumn(mainConfigTable.getColumnModel().getColumn(2));
		thresholdConfigTable = new JTable();
		thresholdConfigTable.setModel(new DefaultTableModel(new Object[][] { { "1", "3", "", "15", "" },
				{ "2", "3.75", "", "15", "" }, { "3", "5", "", "", "" }, { "4", "2.5", "2.5", "", "" },
				{ "5", "2", "2.5", "", "" }, { "6", "6", "6", "", "" }, { "7", "2.5", "2.5", "", "" },
				{ "8", "0", "5", "", "" }, { "9", "3", "", "15", "" }, { "10", "3.75", "", "20", "" },
				{ "11", "5", "", "30", "" }, { "12", "3", "", "15", "" }, { "13", "3.75", "", "20", "" },
				{ "14", "3", "", "15", "" }, { "15", "3.75", "", "20", "" }, { "16", "5", "", "30", "" },
				{ "17", "3", "", "15", "" }, { "18", "3.75", "", "20", "" }, { "19", "3", "", "15", "" },
				{ "20", "3.75", "", "20", "" }, { "21", "5", "", "30", "" }, { "22", "3", "", "15", "" },
				{ "23", "3.75", "", "20", "" }, { "24", "2.5", "", "24.70|13.76", "13.92|9.44" },
				{ "25", "3.75", "", "24.70|13.77", "13.92|9.44" }, { "26", "5.36", "", "24.70|13.78", "13.92|9.44" },
				{ "27", "2.5", "", "24.70|13.76", "13.92|9.44" }, { "28", "3.75", "", "24.70|13.77", "13.92|9.44" },
				{ "29", "5.36", "", "24.70|13.78", "13.92|9.44" }, { "30", "2.5", "", "24.70|13.76", "13.92|9.44" },
				{ "31", "3.75", "", "24.70|13.77", "13.92|9.44" }, { "32", "5.36", "", "24.70|13.78", "13.92|9.44" } },
				new String[] { "No.", "Threshold", "Threshold_M", "gmin", "gmin congested" }) {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			boolean[] columnEditables = new boolean[] { false, true, true, true, true };

			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});

		scrollPane_2.setViewportView(thresholdConfigTable);
		scrollPane_3.setViewportView(mainConfigTable);

		panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.SOUTH);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		btnNewButton = new JButton("Simulate");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String mainConfigString = getMainConfigSaveString();
				System.out.println(mainConfigString);

				double relaxCAV = getDoubleValue(mainConfigString, "relaxCAV");
				double relaxMDV = getDoubleValue(mainConfigString, "relaxMDV");
				double vMax = getDoubleValue(mainConfigString, "vMax");
				double vInit = getDoubleValue(mainConfigString, "vInit");
				
				
				double inputCarRate = (int) getDoubleValue(mainConfigString, "nCar");
				double timeToMonitor = getDoubleValue(mainConfigString, "t");
				
				int nCar = (int) ((inputCarRate / 3600) * timeToMonitor * 2);
				
				
//				int nCar = (int) getDoubleValue(mainConfigString, "nCar");
				double roadLength = getDoubleValue(mainConfigString, "roadLength");
				double laneDrop = getDoubleValue(mainConfigString, "laneDrop");

				double laneChangeCAV = getDoubleValue(mainConfigString, "laneChangeCAV");
				double laneChangeMDV = getDoubleValue(mainConfigString, "laneChangeMDV");
				
				
				double carLength = getDoubleValue(mainConfigString, "carLength");

				double gapFree = getDoubleValue(mainConfigString, "gapFree");

				double gapMinCAV = getDoubleValue(mainConfigString, "gapMinCAV");
				double gapMinMDV = getDoubleValue(mainConfigString, "gapMinMDV");

				double criticalSpace = getDoubleValue(mainConfigString, "sc");
				double cavSensitivity = getDoubleValue(mainConfigString, "cavSensitivity");
				double mdvSensitivity = getDoubleValue(mainConfigString, "mdvSensitivity");

				double minA = getDoubleValue(mainConfigString, "minA");
				double maxA = getDoubleValue(mainConfigString, "maxA");
				double spaceMeanBeforeLD = getDoubleValue(mainConfigString, "spaceMeanBeforeLD");
				double spaceMeanAfterLD = getDoubleValue(mainConfigString, "spaceMeanAfterLD");
				double tAvgDensity = getDoubleValue(mainConfigString, "tAvgDensity");
				
				
				double mdvVsV1 = getDoubleValue(mainConfigString, "mdvVsV1");
				double mdvVsV2 = getDoubleValue(mainConfigString, "mdvVsV2");
				double mdvVsLint = getDoubleValue(mainConfigString, "mdvVsLint");
				double mdvVsBeta = getDoubleValue(mainConfigString, "mdvVsBeta");
				double cavVsP = getDoubleValue(mainConfigString, "cavVsP");
				double cavVsV1 = getDoubleValue(mainConfigString, "cavVsV1");
				double cavVsV2 = getDoubleValue(mainConfigString, "cavVsV2");
				double cavVsLint = getDoubleValue(mainConfigString, "cavVsLint");
				double cavVsBeta = getDoubleValue(mainConfigString, "cavVsBeta");

				String cavMdvRatio = getStringValue(mainConfigString, "cavMdvRatio");
				String vD = getStringValue(mainConfigString, "vD");

				String[] cavMdvSplit = cavMdvRatio.split(":");
				int cav = Integer.parseInt(cavMdvSplit[0]);
				int mdv = Integer.parseInt(cavMdvSplit[1]);

				int cavCar = cav * nCar / (cav + mdv);
				int mdvCar = nCar - cavCar;

				String thresholdConfigsString = getThresholdConfigSaveString();

				String[] thresholdConfigs = thresholdConfigsString.split(System.getProperty("line.separator"));

				double[] thresholds = new double[thresholdConfigs.length];

				double[] thresholds_m = new double[thresholdConfigs.length];
				SdData[] gMins = new SdData[thresholdConfigs.length];
				SdData[] gMinsCongested = new SdData[thresholdConfigs.length];

				for (int i = 0; i < thresholdConfigs.length; i++) {
					String[] thresholdConfig = thresholdConfigs[i].split(",");
					thresholds[i] = Double.parseDouble(thresholdConfig[1]);
					if (thresholdConfig.length >= 4) {
						gMins[i] = new SdData(thresholdConfig[3]);
					}

					if (thresholdConfig.length >= 5) {
						gMinsCongested[i] = new SdData(thresholdConfig[4]);
					}
				}
				
				int outputIntervalInSeconds = ((int) getDoubleValue(mainConfigString, "outputInterval")) * 60;

				SimulatorConfig.SetSimulatorConfig(inputCarRate, vMax, vInit, timeToMonitor, roadLength, laneDrop, laneChangeCAV, laneChangeMDV,
						carLength, relaxCAV, relaxMDV , cavSensitivity, mdvSensitivity, gapFree, gapMinCAV, gapMinMDV, criticalSpace,
						cavCar, mdvCar, thresholds, thresholds_m, gMins, gMinsCongested, minA, maxA,
						new SdData(vD), tAvgDensity, spaceMeanBeforeLD, spaceMeanAfterLD, outputIntervalInSeconds, mdvVsV1, mdvVsV2, mdvVsLint, mdvVsBeta,
						cavVsP, cavVsV1, cavVsV2, cavVsLint, cavVsBeta);
				SimulationResult result = new SimulationResult();

				try {
					result = Simulator.simulateInput(chckbxExportLog.isSelected(), avoidCollisionCheckBox.isSelected(), comboBox.getSelectedIndex() == 1);
				} catch (Exception e) {
					System.out.print(e);
				}

				String pattern = "###,###.###";
				DecimalFormat decimalFormat = new DecimalFormat(pattern);
				System.out.println(decimalFormat.format(result.timeToFinished));

//				result_panel
				Object[][] intervalResults = new Object[result.intervalOutputs.length][8];
				for (int i = 0; i < result.intervalOutputs.length; i++) {
					IntervalOutput intervalOutput = result.intervalOutputs[i];
					if (intervalOutput == null) {
						break;
					}
					intervalResults[i][0] = intervalOutput.timeStamp;
					intervalResults[i][1] = intervalOutput.vehicalCountLD;
					intervalResults[i][2] = intervalOutput.vehicalCountFin;
					intervalResults[i][3] = intervalOutput.throughput;
					intervalResults[i][4] = decimalFormat.format(intervalOutput.speedBeforeLD);
					intervalResults[i][5] = decimalFormat.format(intervalOutput.speedAfterLD);
					
					intervalResults[i][6] = decimalFormat.format(intervalOutput.speedLD);
					intervalResults[i][7] = decimalFormat.format(intervalOutput.speedFin);
					
				}
				
				table.setModel(new DefaultTableModel(
						intervalResults,
						new String[] { "Timestamp", "VehCnt LD", "VehCnt Fin", "Throughput", "Spd Before LD", "Spd After LD", "Spd LD", "Spd Fin" }) {
					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;
					boolean[] columnEditables = new boolean[] { false, false, false, false };

					public boolean isCellEditable(int row, int column) {
						return columnEditables[column];
					}
				});
				
				double totalTime = result.lastCarTimeStamp - result.firstCarTimeStamp;
				Object[][] resultData = new Object[result.averageSpeedAtDetectionPoint.length + 4][3];
//				{
////					{"First Car arrived At (s): ", decimalFormat.format(result.firstCarTimeStamp)},
////					{"Last Car arrived At (s): ", decimalFormat.format(result.lastCarTimeStamp)},
////					{"Total Time (s) (LastCar - FirstCar): ", decimalFormat.format(totalTime)},
//					
//					{"Avg Speed at Detection Point, Lane 1 (m/s): ", decimalFormat.format(result.averageSpeedAtDetectionPoint[0])},
//					{"Avg Speed at Detection Point, Lane 2 (m/s): ", decimalFormat.format(result.averageSpeedAtDetectionPoint[1])},
//					{"Throughput (Veh/hour/lane): ", decimalFormat.format(result.throughput)},
//					{"Avg Density at detection point(Veh/km/lane): ", (new DecimalFormat("###,###.#")).format(result.avgDensities)},
//					{"Avg Time to Detection point (s): ", decimalFormat.format(result.averageTimeToBottleNeck)},
//					{"Avg Time to Finished (s): ", decimalFormat.format(result.averageTimeToFinished)},
//					{"Avg Speed at Lane drop (m/s): ", decimalFormat.format(result.averageSpeedAtLanedrop)},
//								
//				};
				
				for (int i = 0; i < result.averageSpeedAtDetectionPoint.length - 2; i++) {
					resultData[i] = new Object[] {"Avg Speed at Detection Point, Lane " + (i + 1) +" (m/s): ", decimalFormat.format(result.averageSpeedAtDetectionPoint[i])};
				}
				int l = result.averageSpeedAtDetectionPoint.length - 2;
				resultData[l] = new Object[] {"Avg Speed at Detection Point, Lane " + (l + 1) +" (m/s): ", decimalFormat.format(result.averageSpeedAtDetectionPoint[l])};
				resultData[l + 1] = new Object[] {"Throughput (Veh/hour/lane): ", decimalFormat.format(result.throughput)};
				resultData[l + 2] = new Object[] {"Avg Density at detection point(Veh/km/lane): ", (new DecimalFormat("###,###.#")).format(result.avgDensities)};
				resultData[l + 3] = new Object[] {"Avg Time to Detection point (s): ", decimalFormat.format(result.averageTimeToBottleNeck)};
				resultData[l + 4] = new Object[] {"Avg Time to Finished (s): ", decimalFormat.format(result.averageTimeToFinished)};
				resultData[l + 5] = new Object[] {"Avg Speed at Lane drop (m/s): ", decimalFormat.format(result.averageSpeedAtLanedrop)};

//				lblNewLabel2.setText("<html>" + sb2.toString() + "</html>");
				
				resultTable.setModel(new DefaultTableModel(
						resultData,
						new String[] { "Data", "Value" }) {
					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;
					boolean[] columnEditables = new boolean[] { false, false };

					public boolean isCellEditable(int row, int column) {
						return columnEditables[column];
					}
				});
			}
		});
		comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"3-to-2", "4-to-3"}));
		comboBox.setSelectedIndex(0);
		panel.add(comboBox);
		btnNewButton.setHorizontalAlignment(SwingConstants.LEFT);
		panel.add(btnNewButton);

		chckbxExportLog = new JCheckBox("Export log");
		panel.add(chckbxExportLog);

		avoidCollisionCheckBox = new JCheckBox("Avoid collision");
		avoidCollisionCheckBox.setSelected(true);
		panel.add(avoidCollisionCheckBox);
		
		splitPane_1 = new JSplitPane();
		frame.getContentPane().add(splitPane_1, BorderLayout.CENTER);
		splitPane_1.setDividerLocation(580);
		scrollPane = new JScrollPane();
		
//		frame.getContentPane().add(scrollPane, BorderLayout.WEST);
		splitPane_1.setLeftComponent(scrollPane);
		
		table = new JTable();
//		frame.getContentPane().add(table, BorderLayout.EAST);
		scrollPane.setViewportView(table);
		
		
		
		
		
		scrollPane_1 = new JScrollPane();
//		frame.getContentPane().add(scrollPane_1, BorderLayout.WEST);
		splitPane_1.setRightComponent(scrollPane_1);
		menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);

		resultTable = new JTable();
//		frame.getContentPane().add(table, BorderLayout.EAST);
		scrollPane_1.setViewportView(resultTable);
		
		mnNewMenu = new JMenu("File");
		menuBar.add(mnNewMenu);

		mntmNewMenuItem = new JMenuItem("Save Config");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int returnVal = fc.showSaveDialog(splitPane);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fc.getSelectedFile();
					System.out.print(file.getPath() + file.getName());

					try {
						String path = file.getPath();

						if (!path.toLowerCase().endsWith(".rsconf")) {
							path = path + ".rsconf";
						}

						FileWriter fw = new FileWriter(path);
						fw.write(getSaveString());
						fw.close();
					} catch (Exception e) {
						System.out.println(e);
					}

				} else {
				}
			}
		});
		mnNewMenu.add(mntmNewMenuItem);

		mntmNewMenuItem_1 = new JMenuItem("Load Config");
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int returnVal = fc.showOpenDialog(splitPane);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fc.getSelectedFile();
					System.out.print(file.getName());
					BufferedReader br;
					try {
						br = new BufferedReader(new FileReader(file));
						String st;
						int row = 0;
						StringBuilder mainConfig = new StringBuilder();
						StringBuilder thresholdConfig = new StringBuilder();
						StringBuilder using = mainConfig;
						while ((st = br.readLine()) != null) {

							if (st.equals("main")) {
								using = mainConfig;
							} else if (st.equals("threshold")) {
								using = thresholdConfig;
							} else {
								String[] splitted = st.split(",");
								for (int i = 0; i < splitted.length; i++) {
//									thresholdConfigTable.setValueAt(splitted[i], row, i);
									using.append(splitted[i]);
									using.append(",");
								}
								using.append(System.getProperty("line.separator"));
							}

							row++;

						}

						String[] mainSettings = mainConfig.toString().split(System.getProperty("line.separator"));
						for (int r = 0; r < mainSettings.length; r++) {
							String[] mainSetting = mainSettings[r].split(",");
							for (int c = 1; c < mainSetting.length - 1; c++) {
								mainConfigTable.setValueAt(mainSetting[c], r, c);
							}
						}

						String[] thresholdSettings = thresholdConfig.toString()
								.split(System.getProperty("line.separator"));
						for (int r = 0; r < thresholdSettings.length; r++) {
							String[] thresholdSetting = thresholdSettings[r].split(",");
							for (int c = 1; c < thresholdSetting.length; c++) {
								thresholdConfigTable.setValueAt(thresholdSetting[c], r, c);
							}
						}

					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				}
				TableModel model = thresholdConfigTable.getModel();
				StringBuilder saveString = new StringBuilder();

				for (int row = 0; row < model.getRowCount(); row++) {
					for (int col = 0; col < model.getColumnCount(); col++) {
						Object val = model.getValueAt(row, col);
						saveString.append(val.toString());
						saveString.append(",");
					}
					saveString.append(System.getProperty("line.separator"));
//					System.out.println(model.getValueAt(row, 0).toString());
				}

				try {
					FileWriter fw = new FileWriter("D:\\ss.txt");
					fw.write(saveString.toString());
					fw.close();
				} catch (Exception e) {
					System.out.println(e);
				}
			}
		});

		mnNewMenu.add(mntmNewMenuItem_1);
	}

	private static String getMainConfigSaveString() {
		StringBuilder saveString = new StringBuilder();
		TableModel mainConfigModel = mainConfigTable.getModel();
		for (int row = 0; row < mainConfigModel.getRowCount(); row++) {
			for (int col = 0; col < mainConfigModel.getColumnCount(); col++) {
				Object val = mainConfigModel.getValueAt(row, col);
				saveString.append(val.toString());
				saveString.append(",");
			}
			saveString.append(System.getProperty("line.separator"));
		}
		return saveString.toString();
	}

	private static String getThresholdConfigSaveString() {
		StringBuilder saveString = new StringBuilder();
		TableModel thresholdModel = thresholdConfigTable.getModel();
		for (int row = 0; row < thresholdModel.getRowCount(); row++) {
			for (int col = 0; col < thresholdModel.getColumnCount(); col++) {
				Object val = thresholdModel.getValueAt(row, col);
				saveString.append(val.toString());
				saveString.append(",");
			}
			saveString.append(System.getProperty("line.separator"));
		}
		return saveString.toString();
	}

	private static String getSaveString() {
		StringBuilder saveString = new StringBuilder();

//        TableModel mainConfigModel = mainConfigTable.getModel();
		saveString.append("main");
		saveString.append(System.getProperty("line.separator"));

		saveString.append(getMainConfigSaveString());

		saveString.append("threshold");
		saveString.append(System.getProperty("line.separator"));

		saveString.append(getThresholdConfigSaveString());

		return saveString.toString();
	}

	private double getDoubleValue(String configsString, String var) {

		String[] configString = configsString.split(System.getProperty("line.separator"));

		for (int i = 0; i < configString.length; i++) {
			String[] splitted = configString[i].split(",");

			if (splitted.length < 3) {
				continue;
			}

			if (splitted[2].equals(var)) {
				return Double.parseDouble(splitted[1]);
			}
		}

		return 0;

	}

	private String getStringValue(String configsString, String var) {

		String[] configString = configsString.split(System.getProperty("line.separator"));

		for (int i = 0; i < configString.length; i++) {
			String[] splitted = configString[i].split(",");

			if (splitted.length < 3) {
				continue;
			}

			if (splitted[2].equals(var)) {
				return splitted[1];
			}
		}

		return "";

	}

}
