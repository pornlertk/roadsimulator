import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
public class Lane {

	List<Car> cars = new ArrayList<Car>();
	
	public int releasedCar;
	
	public double speedLimit;
	public double monitorLength;
	public double dropAt;
	
	public ArrayList<Double> speedAtDetectionPoint = new ArrayList<Double>();

	public ArrayList<Double> spdAfterLD = new ArrayList<Double>();

	public int spdLdIndex = 0;
	public double accSpdLd = 0;
	
	public Lane (double monitorLength, double dropAt, double speedLimit) {
		this.monitorLength = monitorLength;
		this.speedLimit = speedLimit;
		this.dropAt = dropAt;
		this.releasedCar = 0;
	}
	
	public void addSpdAfterLD (double spdAfterLd) {
		this.accSpdLd += 1 / spdAfterLd;
		this.spdLdIndex++;
	}
	
//	public double getAverageSpeedAtDetectionPoint () {
//		
//		int nPassingCar = this.speedAtDetectionPoint.size();
//		
//		if (nPassingCar == 0) {
//			return 0;
//		}
//		
//		double vAcc = 0;
//		Iterator<Double> speedIterator = this.speedAtDetectionPoint.iterator();
//		while(speedIterator.hasNext()) {
//			double v = speedIterator.next();
//			vAcc += v;
//		}
//		return vAcc / nPassingCar;
//	}
	
	public double getAverageSpaceMeanSpeed () {
		
		int nPassingCar = this.spdAfterLD.size();
		
		if (nPassingCar == 0) {
			return 0;
		}
		
		double vAcc = 0;
		Iterator<Double> speedIterator = this.spdAfterLD.iterator();
		while(speedIterator.hasNext()) {
			double v = speedIterator.next();
			vAcc += v;
		}
		return vAcc / nPassingCar;
	}
	
	public double getAvgVelocity () {
		Iterator<Car> carsIterator = this.cars.iterator();
		double avgV = 0;
		
		while(carsIterator.hasNext()) {
			Car fCar = carsIterator.next();
			avgV += fCar.v;
		}
		
		int carInLane = this.cars.size();
		if (carInLane == 0) {
			return 0;
		}
		
		return avgV / this.cars.size();
		
	}
	
	public boolean isAbleToReleaseNewCar () {
		int carInLane = this.cars.size();
		if (carInLane == 0) {
			return true;
		}
		
		Car lastCarInLane = this.cars.get(carInLane - 1);
		return lastCarInLane.x > lastCarInLane.length;
	}
	
	public int findFittedIndexByDistance (double distance) {
		int index = 0;
		Iterator<Car> carIterator = this.cars.iterator();
		while (carIterator.hasNext()) {
			Car car = carIterator.next();
			if (distance > car.x) {
				break;
			}
			else {
				index++;
			}
		}
		
		
		return index;
	}
}
