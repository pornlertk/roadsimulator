import java.util.Random;

public class ReleaseSchedule {
	
	
	int[][] laneTimeSchedule;

	CarType[][] carTypeSchedule;
	
	public ReleaseSchedule(int nLanes, int nCavCar, int nMdvCar, double timeToMonitor, double carRate) {
		laneTimeSchedule = getPoissonDistReleaseSchedule(nLanes, nCavCar, nMdvCar, timeToMonitor, carRate);
		carTypeSchedule = getCarTypeSchedule(nLanes, nCavCar, nMdvCar, timeToMonitor);
	}

	
	private int[][] getPoissonDistReleaseSchedule (int nLanes, int nCavCar, int nMdvCar, double timeToMonitor, double carRate) {
		int nCar = nCavCar + nMdvCar;


		
		int[][] releaseSchedule = new int[nLanes][nCar];
		int[] distCounter = new int[nLanes];		

		for (int i = 0; i < nLanes - 1; i++) {
			distCounter[i] = 0;
		}
		distCounter[nLanes - 1] = 0;
		
		for (int i = 0; i < nCar; i++) {
			for (int y = 0; y < nLanes; y++) {
				
				int tChange = 1;
				if (timeToMonitor - distCounter[y] > nCar - i) {
					int poissonRandom = getPoissonRandomByLn(carRate);
					tChange = poissonRandom;
				}
				
				distCounter[y] = distCounter[y] + tChange;
				releaseSchedule[y][i] = distCounter[y];
			}
		}
		
		return releaseSchedule;
	}
	
	private CarType[][] getCarTypeSchedule (int nLanes, int nCavCar, int nMdvCar, double timeToMonitor) {
		int nCar = nCavCar + nMdvCar;
		CarType[][] schedule = new CarType[nLanes][nCar];
		
		for (int x = 0; x < nLanes; x++) {
			for (int i = 0; i < nCar; i++) {
				if (i < nCavCar) {
					schedule[x][i] = CarType.CAV;
				}
				else {
					schedule[x][i] = CarType.MDV;
				}
			}
			schedule[x] = RandomizeArray(schedule[x]);
		}
		
		
		return schedule;
	}
	
	private static int getPoissonRandomByLn(double vehiclePerHour) {

//		double raw = -Math.log(1 - Math.random()) * timeInSecond / nCar;
		//t = interarrival time = -ln(1 � Random Number)/lambda
		double raw = (-Math.log(1 - Math.random()) / vehiclePerHour) * 3600;
		int result = (int) Math.round(raw);
		
		return result == 0 ? 1 : result;
	}
	

	public static <T> T[] RandomizeArray(T[] array){
		Random rgen = new Random();  // Random number generator			
 
		for (int i=0; i<array.length; i++) {
		    int randomPosition = rgen.nextInt(array.length);
		    T temp = array[i];
		    array[i] = array[randomPosition];
		    array[randomPosition] = temp;
		}
 
		return array;
	}

	
}
