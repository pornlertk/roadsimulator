import java.util.List;
import java.util.ArrayList;
import java.util.Comparator;
public class Car {


	public int id;
	public List<Double> histX = new ArrayList<Double>();
	public List<Double> histT = new ArrayList<Double>();
	public List<Integer> histLane = new ArrayList<Integer>();
//	public List<Double> histV = new ArrayList<Double>();
//	public List<Double> histA = new ArrayList<Double>();
	public double x;
	public double y;
	public double vD;
	private double a;
	public double v;

	public double u;
	
	public double length = 4.5; // metres
	
	public double sensitivityFactor; // metres
	
	public double startTimeStamp;
	public double detectedTimeStamp;
	public double finishedTimeStamp;
	
	public double speedAtDetectPoint;
	public double speedAtFinishedPoint;
	
	public CarType carType;
	
	public int laneOrder;
	public int laneToChangeOrder;
	
	public int lane;
	public int laneToChange = -1;

	public int startInLane;
	
	public double detectedAt;
	public double spdAfterLD;
	public double spdBeforeLD;
	

	public double xBeforeLD;
	public double beforeTimeStamp;
	

	public Boolean isDetectedSpeedBeforeLD = false;
	
	public Boolean isDetected = false;
	public Boolean isFinished = false;
	
	public Boolean isChangedLane = false;
	

	public Boolean isPlatoonLeading = false;

	public Boolean isCounted = false;
	
	
	
	public List<CarLog> log = new ArrayList<CarLog>();
	
	public void setAcceleratorRate(double a) {
		this.a = a;
	}
	public double getAcceleratorRate() {
		return this.a;
	}
	
	public double getDetectedTime() {
		return this.detectedTimeStamp - this.startTimeStamp;
	}
	
	public double getFinishedTime() {
		return this.finishedTimeStamp - this.startTimeStamp;
	}

	public Car (int id, double v, double x, int lane, double length, double sensitivityFactor, CarType carType, double startTimeStamp) {
		this.id = id;
		this.u = v;
		this.x = x;
		this.lane = lane;
		this.length = length;
		this.sensitivityFactor = sensitivityFactor;
		this.carType = carType;
		this.startTimeStamp = startTimeStamp;
		this.startInLane = lane;
				
	}
//	@Override
//	public int compareTo(Object arg0) {
//		return 1;
////		if (arg0 == null) {
////			return 1;
////		}
////		Car c = (Car) arg0;
////		return this.x >= c.x ? 1 : 0;
//	}
	
}

class MDVCar extends Car {
	
	

	public MDVCar(int id, double v, double x, int lane, double startTimeStamp) {
		super(id, v, x, lane, SimulatorConfig.carLength, SimulatorConfig.mdvSensitivity, CarType.MDV, startTimeStamp);
		this.vD = SimulatorConfig.vD.getValue();
	}
	
}

class CAVCar extends Car {
	public int tGapCreation = -1;
	public CAVCar(int id, double v, double x, int lane, double startTimeStamp) {
		super(id, v, x, lane, SimulatorConfig.carLength, SimulatorConfig.cavSensitivity, CarType.CAV, startTimeStamp);
		this.vD = SimulatorConfig.vMax;
	}
}


class LaneChangeInfo {
	public Car changingLaneCar;
	public double timeRemaining;
	
	public LaneChangeInfo (Car car, double timeNeed) {
		this.changingLaneCar = car;
		this.timeRemaining = timeNeed;
	}
}

class SortByDistance implements Comparator<Car> 
{ 
    // Used for sorting in ascending order of 
    // roll number 
    public int compare(Car a, Car b) 
    { 
    	if (a == null && b == null) {
    		return 0;
    	}
    	else if (a == null && b != null) {
    		return 1;
    	}
    	else if (b == null && a != null) {
    		return -1;
    	}
    	else if (a.x == b.x) {
    		return 0;
    	}
    	
    	
    	return a.x > b.x ? -1 : 1; 
    } 
} 

class SortById implements Comparator<Car> 
{ 
    // Used for sorting in ascending order of 
    // roll number 
    public int compare(Car a, Car b) 
    { 
    	if (a == null && b == null) {
    		return 0;
    	}
    	else if (a == null && b != null) {
    		return -1;
    	}
    	else if (b == null && a != null) {
    		return 1;
    	}
    	else if (a.id == b.id) {
    		return 0;
    	}
    	
    	
    	return a.id > b.id ? 1 : -1; 
    } 
} 

