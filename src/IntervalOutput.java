
public class IntervalOutput {
	
	public double timeStamp;
	public int vehicalCountLD;
	public int vehicalCountFin;
	

	public int finishedVehicalCount;
	
	public double density;
	public double throughput;
	
	public double speedBeforeLD;
	public double speedAfterLD;
	
	public double speedLD;
	public double speedFin;
	
	public int interval;
	
	public IntervalOutput(
			double timeStamp, 
			int interval, 
			int vehicalCountLD, 
			int vehicalCountFin, 
			double density, 
			double throughput, 
			double beforeLDSpeed, 
			double afterLDSpeed,
			double speedLd,
			double speedFin) {
		this.timeStamp = timeStamp;
		this.vehicalCountLD = vehicalCountLD;
		this.vehicalCountFin = vehicalCountFin;
		this.density = density;
		this.throughput = throughput;
		this.interval = interval;
		this.speedAfterLD = beforeLDSpeed;
		this.speedBeforeLD = afterLDSpeed;
		
		this.speedLD = speedLd;
		this.speedFin = speedFin;
	}

}

