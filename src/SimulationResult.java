
public class SimulationResult {

	public double timeToFinished;
	public double[] averageSpeedAtDetectionPoint;
	
	public double averageTimeToBottleNeck;
	public double averageTimeToFinished;
	public double averageSpeedAtLanedrop;
	

	public double throughputAt;
	

	public double firstCarTimeStamp;
	public double lastCarTimeStamp;
	

	public double avgDensities;
	
	public double throughput;
	
	public int nCar;
	
	public Car[] allCars;
	
	public IntervalOutput[] intervalOutputs;
	
	SimulationResult() {
		
	}
	
	SimulationResult (
			double timeToFinished,
			double[] averageSpeedAtDetectionPoint,
			double averageTimeToBottleNeck,
			double averageTimeToFinished,
			double averageSpeedAtLanedrop,
			double throughputAt,
			double throughput,
			int nCar,
			double avgDensities,
			double firstCarTimeStamp,
			double lastCarTimeStamp,
			IntervalOutput[] intervalOutputs
			) {
		this.timeToFinished = timeToFinished;
		this.averageSpeedAtDetectionPoint = averageSpeedAtDetectionPoint;

		this.averageTimeToBottleNeck = averageTimeToBottleNeck;
		this.averageTimeToFinished = averageTimeToFinished;
		this.averageSpeedAtLanedrop = averageSpeedAtLanedrop;

		this.throughputAt = throughputAt;
		this.throughput = throughput;
		
		this.nCar = nCar;
		
		this.avgDensities = avgDensities;
		

		this.firstCarTimeStamp = firstCarTimeStamp;
		this.lastCarTimeStamp = lastCarTimeStamp;
		this.intervalOutputs = intervalOutputs;
	}
	
}


