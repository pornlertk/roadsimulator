import java.util.*;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Simulator {

	private static Map<Car, Double> changingLanes;
	public static SimulationResult simulateInput(boolean isExportCsv, boolean avoidCollision, boolean isFourToThree)
			throws IOException {

		StringBuilder log = new StringBuilder();

		double tChange = 1;
		int detectedCar = 0;
		// int finCar = 0;

		double throughput = 0;
		double t = 0;

		double roadLength = SimulatorConfig.roadLength;
		double vMax = SimulatorConfig.vMax;
		double detectPosition = SimulatorConfig.laneDrop;

		int nLanes = isFourToThree ? 4 : 3;

		Lane[] lanes = new Lane[nLanes];

		for (int i = 0; i < nLanes - 1; i++) {
			lanes[i] = new Lane(roadLength, Double.MAX_VALUE, vMax);
		}
		lanes[nLanes - 1] = new Lane(detectPosition, detectPosition, vMax);

		// Number of car to simulate

		int nCavCar = SimulatorConfig.cavCar;
		int nMdvCar = SimulatorConfig.mdvCar;

		double timeToMonitor = SimulatorConfig.timeToMonitor;

		ReleaseSchedule releaseScheduler = new ReleaseSchedule(lanes.length, nCavCar, nMdvCar, timeToMonitor,
				SimulatorConfig.inputCarRate);

		Car[] allCars = new Car[(nCavCar + nMdvCar) * lanes.length];
		Car[] allCarsById = new Car[(nCavCar + nMdvCar) * lanes.length];

		int counter = 0;

		double firstCarDetectedTimestamp = 0;
		double lastCarDetectedTimestamp = 0;

		Simulator.changingLanes = new Hashtable<Car, Double>();

		List<Double> densities = new ArrayList<Double>();

		int detectedCarAtStartInspectTime = 0;
//		int detectedCarAtEndInspectTime = 0;

		double startInspectTime = SimulatorConfig.startInspectTime;
//		double endInspectTime = -1;
		boolean collectingData = true;

		IntervalOutput[] intervalOutputs = new IntervalOutput[(int) Math
				.floor(timeToMonitor / SimulatorConfig.outputInterval)];


		int intervalCnt = 0;

		List<Double> speedAfterLDs = new ArrayList<Double>();
		List<Double> speedBeforeLDs = new ArrayList<Double>();
		
//		ArrayList<Double> spdLd = new ArrayList<Double>();
//		ArrayList<Double> spdFin = new ArrayList<Double>();
		int spdLdIndex = 0;
		int spdFinIndex = 0;
		
		double spdLdValue = 0;
		double spdFinValue = 0;
		
		while (t < timeToMonitor) {

			// Update car position

			for (int i = 0; i < allCars.length; i++) {
				Car focusedCar = allCars[i];

				// if get null, after that will always be null, so we break the loop
				if (focusedCar == null) {
					break;
				}

				// Distance that gain in tChange time frame [ s = ut + (1/2)at^2]
				double distance = (focusedCar.v * tChange)
						+ ((0.5) * focusedCar.getAcceleratorRate() * Math.pow(tChange, 2));

				double criticalSpace = SimulatorConfig.criticalSpace;


				/////////////////////////////////////////////////////

				double spaceHeadway = getSpaceAhead(focusedCar, lanes[focusedCar.lane]);

				if (avoidCollision) {
					if (distance < 0) {
						System.out.println("No REVERSE: SET V to 0");
						distance = 0;
					}
					if (distance >= spaceHeadway) {
						System.out.println("Space ahead is not enough to advance: SET V to 0");
						distance = 0;
					}
				}

				if (isExportCsv) {
					focusedCar.histT.add(t);
				}

				// update X position

				focusedCar.x = focusedCar.x + distance;

				// update v as (v = u + at) or v[t] = v[t-1] + a[t-1] * deltaT
				focusedCar.u = focusedCar.v;
				focusedCar.v = focusedCar.u + (focusedCar.getAcceleratorRate() * tChange);

				if (focusedCar.v < 0) {
					focusedCar.v = 0;
				}

				// Check if is platoon leading, since it's affect acceleration
				focusedCar.isPlatoonLeading = isPlatoonLeading(lanes[focusedCar.lane], focusedCar, criticalSpace);

				// update a
				// double k = focusedCar.carType == CarType.CAV ? SimulatorConfig.kCAV : SimulatorConfig.kMDV;
				double a = calculateAccelerator(lanes, focusedCar, criticalSpace, avoidCollision);
				focusedCar.setAcceleratorRate(a);

				if (isExportCsv) {

					if (focusedCar.x <= SimulatorConfig.roadLength) {
						focusedCar.histX.add(focusedCar.x);
						focusedCar.histLane.add(focusedCar.lane);
					}

//					focusedCar.histV.add(focusedCar.v);
//					focusedCar.histA.add(a);
				}

				// if reached detection point, accumulate counter and mark as detected
				if (focusedCar.x >= detectPosition && !focusedCar.isDetected) {

					lanes[focusedCar.lane].speedAtDetectionPoint.add(focusedCar.v);
					focusedCar.isDetected = true;
					focusedCar.detectedTimeStamp = t;
					focusedCar.speedAtDetectPoint = focusedCar.v;
					focusedCar.detectedAt = focusedCar.x;
					detectedCar++;
					
					spdLdValue += (1 / (focusedCar.x / (t - focusedCar.startTimeStamp)));
					spdLdIndex++;

					if (detectedCar == 1) {
						firstCarDetectedTimestamp = t;
					}
					if (detectedCar == ((SimulatorConfig.cavCar + SimulatorConfig.mdvCar) * lanes.length)) {
						lastCarDetectedTimestamp = t;
					}
				}
				
				if (focusedCar.x >= detectPosition - SimulatorConfig.spaceMeanBeforeLD && !focusedCar.isDetectedSpeedBeforeLD) {
					// !!! Calculate space meanspeed !!!

					focusedCar.xBeforeLD = focusedCar.x;
					focusedCar.beforeTimeStamp = t;
					
					focusedCar.isDetectedSpeedBeforeLD = true;
				}


				if (focusedCar.x >= detectPosition + SimulatorConfig.spaceMeanAfterLD && focusedCar.isDetected) {
					// !!! Calculate space meanspeed !!!

					double x = focusedCar.x - focusedCar.detectedAt;
					double time = t - focusedCar.detectedTimeStamp;
					focusedCar.spdAfterLD = x / time;
//					lanes[focusedCar.lane].spdAfterLD.add(focusedCar.spdAfterLD);
					
					lanes[focusedCar.lane].addSpdAfterLD(focusedCar.spdAfterLD);
					
					speedAfterLDs.add(focusedCar.spdAfterLD);
					
					double xBefore = focusedCar.detectedAt - focusedCar.xBeforeLD;
					double beforeTime = focusedCar.detectedTimeStamp - focusedCar.beforeTimeStamp;
					focusedCar.spdBeforeLD = xBefore / beforeTime;
					speedBeforeLDs.add(focusedCar.spdBeforeLD);
				}

				// if reached finished point, mark as finished
				if (focusedCar.x >= lanes[focusedCar.lane].monitorLength && !focusedCar.isFinished) {
					logMessage(log, "Car " + focusedCar.id + " reached detection point in lane" + focusedCar.lane, t);
					focusedCar.isFinished = true;
					focusedCar.finishedTimeStamp = t;
					focusedCar.speedAtFinishedPoint = focusedCar.v;
					spdFinValue += (1 / (focusedCar.x / (t - focusedCar.startTimeStamp)));
					spdFinIndex++;
				}

			}

			// Sort cars by order
			Arrays.sort(allCars, new SortByDistance());

			// End Update car position

			double density = 0;
			// Calculate Throughput and densities
			if (t >= startInspectTime && collectingData) {

				if (t == startInspectTime) {
					detectedCarAtStartInspectTime = detectedCar;
				}

				if (counter == allCars.length) {
//					endInspectTime = t;
//					detectedCarAtEndInspectTime = detectedCar;
					collectingData = false;
				} else {
					int carPerKm = getNumCarInRange(allCars, detectPosition, 1000);
					density = carPerKm / lanes.length;
					densities.add(density);
				}
			}

			if (t != 0 && t % SimulatorConfig.outputInterval == 0) {

//				int ldCnt = spdLd.size();
//				int finCnt = spdFin.size();
				int ldCnt = spdLdIndex;
				int finCnt = spdFinIndex;
				
				
				int occ = getNumCarInRange(allCars, detectPosition, 1000) / lanes.length;
				int vph = (3600 * ldCnt) / SimulatorConfig.outputInterval;

				Iterator<Double> speedAfterLDsIterator = speedAfterLDs.iterator();
				Iterator<Double> speedBeforeLDsIterator = speedBeforeLDs.iterator();
				
				int afterLDIndex = 0;
				double afterLDValue = 0;
				int beforeLDIndex = 0;
				double beforeLDValue = 0;
				
//				ArrayList<Double> speedAfterLDSerie = new ArrayList<Double>();
//				ArrayList<Double> speedBeforeLDSerie = new ArrayList<Double>();
				while (speedAfterLDsIterator.hasNext()) {
					double sms = speedAfterLDsIterator.next();
					afterLDValue += (1 / sms);
//					speedAfterLDSerie.add(sms);
					afterLDIndex++;
				}
				
				while (speedBeforeLDsIterator.hasNext()) {
					double sms = speedBeforeLDsIterator.next();
					beforeLDValue += (1 / sms);
//					speedBeforeLDSerie.add(sms);
					beforeLDIndex++;
				}

//				double avgSpeedAfterLD = GetHarmonicMean(speedAfterLDSerie);
//				double avgSpeedBeforeLD = GetHarmonicMean(speedBeforeLDSerie);
				double avgSpeedAfterLD = afterLDIndex / afterLDValue;
				double avgSpeedBeforeLD = beforeLDIndex / beforeLDValue;
				
//				double avgSpeedLD = GetHarmonicMean(spdLd);
//				double avgSpeedFin = GetHarmonicMean(spdFin);
				
				double avgSpeedLD = spdLdIndex / spdLdValue;
				double avgSpeedFin = spdFinIndex / spdFinValue;
				
				spdLdValue = 0;
				spdFinValue = 0;
				spdLdIndex = 0;
				spdFinIndex = 0;
				
				IntervalOutput io = new IntervalOutput(
						t,
						SimulatorConfig.outputInterval,
						ldCnt,
						finCnt,
						occ, 
						vph,
						avgSpeedAfterLD,
						avgSpeedBeforeLD,
						avgSpeedLD,
						avgSpeedFin);

				speedAfterLDs = new ArrayList<Double>();
				intervalOutputs[intervalCnt++] = io;

			}

			// End Calculate Throughput and densities

			// loop through changing lane dictionary to reduce time remain to successfully
			// change lane
			List<Car> doneChangeLane = new ArrayList<Car>();
			for (Map.Entry<Car, Double> entry : Simulator.changingLanes.entrySet()) {
				Car key = entry.getKey();
				double timeRemain = Simulator.changingLanes.get(key) - tChange;
				if (timeRemain <= 0) {
					doneChangeLane.add(key);
				}
				Simulator.changingLanes.replace(key, timeRemain);
			}

			// Remove from changing lane dict
			Iterator<Car> doneChangeLaneIterator = doneChangeLane.iterator();
			while (doneChangeLaneIterator.hasNext()) {
				Car car = doneChangeLaneIterator.next();

				lanes[car.lane].cars.remove(car);
				lanes[car.laneToChange].cars.add(lanes[car.laneToChange].findFittedIndexByDistance(car.x), car);
				car.lane = car.laneToChange;
				car.laneToChange = -1;
				Simulator.changingLanes.remove(car);
			}

			// Loop through lane
			// For check lane changing
			for (int laneIndex = 0; laneIndex < lanes.length; laneIndex++) {

				Lane lane = lanes[laneIndex];
				Car aheadCar = null;
				Iterator<Car> carIterator = lane.cars.iterator();

				// Loop through car to check if car need to change lane
				while (carIterator.hasNext()) {
					Car car = carIterator.next();

					if (Simulator.changingLanes.get(car) != null || car.isFinished || car.x == 0) {
						continue;
					}

					int toTheLeftIndex = laneIndex - 1;
					int toTheRightIndex = laneIndex + 1;

					Lane laneToTheLeft = null;
					if (toTheLeftIndex >= 0 && toTheLeftIndex < lanes.length) {
						laneToTheLeft = lanes[toTheLeftIndex];
					}

					Lane laneToTheRight = null;
					if (toTheRightIndex >= 0 && toTheRightIndex < lanes.length) {
						laneToTheRight = lanes[toTheRightIndex];
					}

					int laneToChange = -1;

					// CHECK IF INSPECTED CAR NEED TO CHANGE LANE
					// Linkage between the Car-Following and the Lane-Changing Models

					double considerLaneChangeAt = car.carType == CarType.CAV ? SimulatorConfig.laneChangeCAV
							: SimulatorConfig.laneChangeMDV;

					// is Platoon-Leading
					if (car.isPlatoonLeading) {
						// Mandatory LC
						if (lane.dropAt - car.x < considerLaneChangeAt) {
							laneToChange = car.lane - 1;
							System.out.println("Car " + car.id + " is tempt to change lane due to lane drop at " + car.v
									+ " m/s. and " + car.getAcceleratorRate() + " m/s2 " + car.x);
						}
					}
					// is Car following
					else {
						// Mandatory LC
						if (lane.dropAt - car.x < considerLaneChangeAt) {
							laneToChange = car.lane - 1;
							System.out.println("Car " + car.id + " is tempt to change lane due to lane drop at " + car.v
									+ " m/s. and " + car.getAcceleratorRate() + " m/s2 " + car.x);
						}
						// Check Discretionary LC
						else {
							boolean needChangeLaneToTheRight = CheckRightLaneChange(car, aheadCar, lane,
									laneToTheRight);
							if (needChangeLaneToTheRight) {
								laneToChange = car.lane + 1;
							} else {
								if (CheckLeftLaneChange(car, aheadCar, lane, laneToTheLeft)) {
									laneToChange = car.lane - 1;
								}
							}
						}
					}

					aheadCar = car;

					// IF NEED TO CHANGE LANE, CHECK IF IT'S CHANGABLE
					if (laneToChange != -1) {
						car.laneToChange = laneToChange;

						boolean isChangeToTheRight = car.laneToChange > car.lane;
						Car changedLaneCar = getFirstCarBeforePosition(lanes[laneToChange], car.x);

						// TODO: If changed lane has no car ?
						if (changedLaneCar == null) {
//							lanes[car.lane].cars.remove(car);
//							lanes[car.laneToChange].cars.add(lanes[car.laneToChange].findFittedIndexByDistance(car.x),
//									car);
//							car.lane = car.laneToChange;
//							car.laneToChange = -1;

							Simulator.changingLanes.put(car, (double) 1);
							continue;
						}

						// CAV-CAV Gap Creation
						if (changedLaneCar.carType == CarType.CAV) {
							CAVCar gapCreationCar = (CAVCar) changedLaneCar;
							boolean gapCreationOccur = false;
							if (car.carType == CarType.CAV) {

								double ccGapCreationThreshold = isChangeToTheRight ? SimulatorConfig.getThreshold(1)
										: SimulatorConfig.getThreshold(2);
								double ccGapCreationGapMean = isChangeToTheRight ? SimulatorConfig.getGMins(1)
										: SimulatorConfig.getGMins(2);

								if (!(gapCreationCar.x - car.x >= car.length + ccGapCreationGapMean
										&& car.v - gapCreationCar.v <= ccGapCreationThreshold)) {
									if (gapCreationCar.v >= 0.8941) {
										Car leadCar = getFirstCarAfterPosition(lanes[gapCreationCar.lane],
												gapCreationCar.x);
										if (leadCar != null) {

											if (leadCar.carType == CarType.CAV) {
												gapCreationCar
														.setAcceleratorRate(leadCar.getAcceleratorRate() - 0.4470);
											} else {
												gapCreationOccur = true;
												gapCreationCar.tGapCreation++;
												double a = (0.5 * (leadCar.u - gapCreationCar.u))
														- (0.8047 * Math.pow(gapCreationCar.tGapCreation, 0.5));
												gapCreationCar.setAcceleratorRate(a);
											}

										}

									} else {
										changedLaneCar.setAcceleratorRate(-changedLaneCar.v);
									}
								}

							}

							if (!gapCreationOccur) {
								gapCreationCar.tGapCreation = -1;
							}
						}

						double gap = car.x - car.length - changedLaneCar.x;
						double gapFree = SimulatorConfig.gapFree;
						double gapMin = car.carType == CarType.CAV ? SimulatorConfig.gapMinCAV
								: SimulatorConfig.gapMinMDV;

						Car aheadCarInChangedLane = getFirstCarAfterPosition(lanes[laneToChange], car.x);
						double space = aheadCarInChangedLane == null ? Double.MAX_VALUE
								: aheadCarInChangedLane.x - car.x;
						if (gap >= gapFree) {

							int[] constIndex = getConstantIndexForConditionalLaneChange(car, changedLaneCar,
									isChangeToTheRight, LaneChangeType.FREE_LC);
							if (CheckLaneChangeCondition(car, changedLaneCar, constIndex[0], constIndex[1],
									IsCongested(density))) {
								int lateralTime = getLateralTime(LaneChangeType.FREE_LC,
										space < SimulatorConfig.criticalSpace);
										Simulator.changingLanes.put(car, (double) lateralTime);
							}

						} else if (gap < gapFree && gap >= gapMin + car.length) {
							int[] constIndex = getConstantIndexForConditionalLaneChange(car, changedLaneCar,
									isChangeToTheRight, LaneChangeType.COOPERATIVE_LC);
							if (CheckLaneChangeCondition(car, changedLaneCar, constIndex[0], constIndex[1],
									IsCongested(density))) {
								int lateralTime = getLateralTime(LaneChangeType.COOPERATIVE_LC, space < SimulatorConfig.criticalSpace);
								Simulator.changingLanes.put(car, (double) lateralTime);
							} else if (car.carType == CarType.MDV || changedLaneCar.carType == CarType.MDV) {
								constIndex = getConstantIndexForConditionalLaneChange(car, changedLaneCar, isChangeToTheRight, LaneChangeType.COMPETITIVE_LC);
								if (CheckCompetitiveLaneChangeCondition(car, changedLaneCar, constIndex[0], constIndex[2], constIndex[1], IsCongested(density))) {
									int lateralTime = getLateralTime(LaneChangeType.COMPETITIVE_LC, space < SimulatorConfig.criticalSpace);
									Simulator.changingLanes.put(car, (double) lateralTime);
								}
							}
						} else if (gap < gapMin + car.length) {

							if (car.carType == CarType.CAV && car.isPlatoonLeading) {

								// Adjust acceleration rate towards the average speed of the target lane

								double relaxationTime = car.carType == CarType.CAV ? SimulatorConfig.relaxCAV : SimulatorConfig.relaxMDV;
								double a = (1 / relaxationTime) * (lanes[laneToChange].getAvgVelocity() - car.v);
								car.setAcceleratorRate(a);
							}

						}

					}
				}

				if (lane.releasedCar < releaseScheduler.laneTimeSchedule[laneIndex].length
						&& releaseScheduler.laneTimeSchedule[laneIndex][lane.releasedCar] == t) {
					boolean canReleaseCar = true;
					if (lane.releasedCar > 0 && lane.cars.size() > 0) {
						Car lastCarInLane = lane.cars.get(lane.cars.size() - 1);
						if (lastCarInLane.x <= lastCarInLane.length) {
							canReleaseCar = false;
							for (int x = lane.releasedCar; x < releaseScheduler.laneTimeSchedule[laneIndex].length; x++) {
								releaseScheduler.laneTimeSchedule[laneIndex][x] += 1;
							}
						}
					}

					if (canReleaseCar) {

						double v = 0;
						if (lane.cars.size() == 0) {
							v = SimulatorConfig.vInit;
						} else {
							v = lane.getAvgVelocity();
						}

						CarType carType = releaseScheduler.carTypeSchedule[laneIndex][lane.releasedCar];

						Car car;
						if (carType == CarType.MDV) {
							car = new MDVCar(counter++, v, 0.0, laneIndex, t);
						} else {
							car = new CAVCar(counter++, v, 0.0, laneIndex, t);
						}

						allCars[car.id] = car;
						allCarsById[car.id] = car;
						lane.cars.add(car);
						lane.releasedCar++;

					}
				}

			}

//			if (t == SimulatorConfig.timeToMonitor) {
//				throughput = detectedCar;
//			}
			t += tChange;
		}

//		StringBuilder sb = new StringBuilder();
		LocalDateTime date = LocalDateTime.now();

		String userHomeFolder = System.getProperty("user.home");
//		File textFile = new File(userHomeFolder, "mytext.txt");

//		throughput = 3600 * (detectedCarAtEndInspectTime - detectedCarAtStartInspectTime) / ((endInspectTime - startInspectTime) * 2);
		throughput = 3600 * (detectedCar - detectedCarAtStartInspectTime) / ((timeToMonitor - startInspectTime) * 2);

		// Write Log
		// /////////////////////////////////////////////////////////////////////////////

		if (isExportCsv) {
			// X
			FileOutputStream fosX = new FileOutputStream(userHomeFolder + "\\Desktop\\X_"
					+ date.format(DateTimeFormatter.ofPattern("yyyy.MM.dd.HH.mm")) + ".csv");
			OutputStreamWriter wX = new OutputStreamWriter(fosX, "UTF-8");
			BufferedWriter bwX = new BufferedWriter(wX);

//			// V
//			FileOutputStream fosV = new FileOutputStream(userHomeFolder + "\\Desktop\\V_" + date.format(DateTimeFormatter.ofPattern("yyyy.MM.dd.HH.mm")) + ".csv");
//			OutputStreamWriter wV = new OutputStreamWriter(fosV, "UTF-8");
//			BufferedWriter bwV = new BufferedWriter(wV);
//			
//			// A
//			FileOutputStream fosA = new FileOutputStream(userHomeFolder + "\\Desktop\\A_" + date.format(DateTimeFormatter.ofPattern("yyyy.MM.dd.HH.mm")) + ".csv");
//			OutputStreamWriter wA = new OutputStreamWriter(fosA, "UTF-8");
//			BufferedWriter bwA = new BufferedWriter(wA);

			// Lane
			FileOutputStream fosL = new FileOutputStream(userHomeFolder + "\\Desktop\\LANE_"
					+ date.format(DateTimeFormatter.ofPattern("yyyy.MM.dd.HH.mm")) + ".csv");
			OutputStreamWriter wL = new OutputStreamWriter(fosL, "UTF-8");
			BufferedWriter bwL = new BufferedWriter(wL);

			Object[] tHeads = new Object[(int) (t / tChange)];

			StringBuilder tHeadSb = new StringBuilder();
			tHeadSb.append("Time,");
			double tCount = 0;
			for (int i = 0; i < tHeads.length; i += tChange) {
				tHeads[i] = tCount;
				tCount += tChange;
			}

//			double startTime = allCars[0].histT.get(0);
			for (int i = 0; i < tHeads.length; i++) {
				String tt = tHeads[i] + "";
				tHeadSb.append(tt);
				tHeadSb.append(",");
			}
			tHeadSb.append(System.getProperty("line.separator"));

			bwX.append(tHeadSb.toString());
//			bwV.append(tHeadSb.toString());
//			bwA.append(tHeadSb.toString());
			bwL.append(tHeadSb.toString());

			Arrays.sort(allCars, new SortById());

			for (int i = 0; i < allCars.length; i++) {
				StringBuilder carStringX = new StringBuilder();
//				StringBuilder carStringA = new StringBuilder();
//				StringBuilder carStringV = new StringBuilder();
				StringBuilder carStringL = new StringBuilder();
				Car c = allCars[i];
				if (c == null || c.histT.size() == 0) {
					continue;
				}
				double startTime = c.histT.get(0);

				carStringX.append("Car " + c.id + ",");
//				carStringA.append("Car " + c.id + ",");
//				carStringV.append("Car " + c.id + ",");
				carStringL.append("Car " + c.id + ",");
				int startIndex = 0;

				startIndex = java.util.Arrays.asList(tHeads).indexOf(startTime);

				int cIndex = 0;
				for (int index = 0; index < tHeads.length; index++) {

					if (index < startIndex) {
						carStringX.append(",");
//						carStringA.append(",");
//						carStringV.append(",");
						carStringL.append(",");
					} else {

						String x = cIndex < c.histX.size() ? c.histX.get(cIndex).toString() : "";
						carStringX.append(x + ",");

//						String a = cIndex < c.histA.size() ? c.histA.get(cIndex).toString() : "";
//						carStringA.append(a + ",");
//						
//						String v = cIndex < c.histV.size() ? c.histV.get(cIndex).toString() : "";
//						carStringV.append(v + ",");

						String l = cIndex < c.histLane.size() ? c.histLane.get(cIndex).toString() : "";
						carStringL.append(l + ",");
						cIndex++;
					}
				}
				carStringX.append(System.getProperty("line.separator"));
//				carStringA.append(System.getProperty("line.separator"));
//				carStringV.append(System.getProperty("line.separator"));

				carStringL.append(System.getProperty("line.separator"));

				bwX.append(carStringX.toString());
//				bwA.append(carStringA.toString());
//				bwV.append(carStringV.toString());
				bwL.append(carStringL.toString());
			}

			bwX.flush();
			bwX.close();

			bwL.flush();
			bwL.close();

//			bwA.flush();
//			bwA.close();

//			bwV.flush();
//			bwV.close();
		}

		// End Write Log

		Iterator<Double> ds = densities.iterator();
		int index = 0;
		double accDs = 0;

//		StringBuilder dsBuilder = new StringBuilder();
		while (ds.hasNext() && index < (densities.size())) {
			double d = ds.next();
//			dsBuilder.append(d + ",");
			accDs += d;
			index++;
		}

		double avgDs = (accDs / (densities.size()));

		double timeToBottleNeck = 0;
		double timeToFinished = 0;
//		double averageSpeedAtLaneDrop = 0; 	
//		double averageSpeedAtLaneDropOneNTwo = 0; 	

		int finishedCar = 0;
		for (int i = 0; i < allCars.length; i++) {
			Car car = allCars[i];

			if (car == null) {
				break;
			}

			if (!car.isDetected) {
				continue;
			}

			double detectedTime = car.getDetectedTime();
			double finishedTime = car.getFinishedTime();
			timeToBottleNeck += detectedTime;
			if (car.isFinished) {
				timeToFinished += finishedTime;
				finishedCar++;
			}

//			averageSpeedAtLaneDrop += car.spaceMeanSpeed;
//			averageSpeedAtLaneDrop += car.speedAtDetectPoint;
		}

		double avgSpeed = GetAvgSpaceMeanSpeed(lanes[0], lanes[1]);

		double[] avgSpaceMeanSpeeds = new double[nLanes];

		for (int i = 0; i < nLanes - 1; i++) {
			avgSpaceMeanSpeeds[i] = lanes[i].getAverageSpaceMeanSpeed();
		}
		avgSpaceMeanSpeeds[nLanes - 1] = lanes[nLanes - 1].getAverageSpaceMeanSpeed();

		SimulationResult result = new SimulationResult(t, avgSpaceMeanSpeeds, timeToBottleNeck / detectedCar,
				timeToFinished / finishedCar,
//				averageSpeedAtLaneDrop / detectedCar,
				avgSpeed,

				SimulatorConfig.timeToMonitor, throughput, (SimulatorConfig.cavCar + SimulatorConfig.mdvCar) * nLanes,
				avgDs, firstCarDetectedTimestamp, lastCarDetectedTimestamp, intervalOutputs);
		return result;
	}

	private static Car getAheadCar(Car focusedCar, Lane lane) {
		int index = lane.cars.indexOf(focusedCar);

		if (index <= 0) {
			return null;
		}

		Car aheadCar = lane.cars.get(index - 1);
		for (Map.Entry<Car, Double> entry : Simulator.changingLanes.entrySet()) {
			Car car = entry.getKey();
			if (car.laneToChange == focusedCar.lane && car.x > focusedCar.x && car.x < aheadCar.x) {
				aheadCar = car;
			}
		}

		return aheadCar;
	}

	private static double getSpaceAhead(Car focusedCar, Lane lane) {

		Car aheadCar = getAheadCar(focusedCar, lane);

		if (aheadCar == null) {
			return lane.dropAt - focusedCar.x;
		}

		return (aheadCar.x - aheadCar.length) - focusedCar.x;

	}

	private static boolean IsCongested(double density) {
		return density >= 25;
	}

	private static boolean CheckLaneChangeCondition(Car inspectCar, Car involvedCar, int threshold_v_index,
			int gapMin_index, boolean isCongested) {
		double threshold_v_constant = SimulatorConfig.getThreshold(threshold_v_index);
		double gapMinConstant = SimulatorConfig.getGMins(gapMin_index);

		if (isCongested) {
			double congestedValue = SimulatorConfig.getGMins_Congested(gapMin_index);
			if (!Double.isNaN(congestedValue)) {
				gapMinConstant = congestedValue;
			}
		}

		return (inspectCar.x - involvedCar.x >= inspectCar.length + gapMinConstant)
				&& (involvedCar.v - inspectCar.v <= threshold_v_constant);
	}

	private static boolean CheckCompetitiveLaneChangeCondition(Car inspectCar, Car involvedCar, int threshold_v_index,
			int threshold_v_min_index, int gapMin_index, boolean isCongested) {
		double threshold_v_constant = SimulatorConfig.getThreshold(threshold_v_index);
		double threshold_v_min_constant = SimulatorConfig.getThreshold(threshold_v_min_index);
		double gapMinConstant = SimulatorConfig.getGMins(gapMin_index);

		if (isCongested) {
			double congestedValue = SimulatorConfig.getGMins_Congested(gapMin_index);
			if (!Double.isNaN(congestedValue)) {
				gapMinConstant = congestedValue;
			}
		}

		return (inspectCar.x - involvedCar.x >= inspectCar.length + gapMinConstant)
				&& (involvedCar.v - inspectCar.v < threshold_v_constant)
				&& (involvedCar.v - inspectCar.v > threshold_v_min_constant);
	}

	private static boolean CheckRightLaneChange(Car car, Car aheadCar, Lane lane, Lane laneToChange) {

		if (aheadCar == null || laneToChange == null) {
			return false;
		}

		if (car.carType == CarType.CAV) {
			return laneToChange.getAvgVelocity() - car.v > SimulatorConfig.getThreshold(3)
					&& laneToChange.getAvgVelocity() - lane.getAvgVelocity() > SimulatorConfig.getThreshold(4)
					&& aheadCar.v - car.v > SimulatorConfig.getThreshold(5);
		} else if (car.carType == CarType.MDV) {
			return laneToChange.getAvgVelocity() - lane.getAvgVelocity() > SimulatorConfig.getThresholdM(4)
					&& aheadCar.v - car.vD > SimulatorConfig.getThresholdM(5);
		}

		return false;
	}

	private static boolean CheckLeftLaneChange(Car car, Car aheadCar, Lane lane, Lane laneToChange) {
		if (aheadCar == null || laneToChange == null) {
			return false;
		}

		if (car.carType == CarType.CAV) {
			return lane.speedLimit - car.v > SimulatorConfig.getThreshold(6)
					&& laneToChange.getAvgVelocity() - lane.getAvgVelocity() > SimulatorConfig.getThreshold(7)
					&& aheadCar.v - car.v < SimulatorConfig.getThreshold(8);
		} else if (car.carType == CarType.MDV) {
			return lane.speedLimit - car.v > SimulatorConfig.getThresholdM(6)
					&& laneToChange.getAvgVelocity() - lane.getAvgVelocity() > SimulatorConfig.getThresholdM(7)
					&& car.vD - aheadCar.v < SimulatorConfig.getThresholdM(8);
		}

		return false;
	}

//	private static double getGapInLaneAtSamePosition(Lane lane, Car car) {
//
//		Car leadCar = null;
//		Car followedCar = null;
//		Car placeHolder = null;
//
//		Iterator<Car> cars = lane.cars.iterator();
//		while (cars.hasNext()) {
//			Car c = cars.next();
//
//			if (c.x <= car.x) {
//				followedCar = c;
//				break;
//			}
//			placeHolder = c;
//		}
//
//		// This mean all cars in lane to change to. is ahead of focused car.
//		if (followedCar == null && placeHolder != null) {
//			// TODO: Discuss with Pond
//			return 0;
//		}
//
//		// This mean all cars in lane to change to. is following focused car.
//		else if (followedCar != null && placeHolder == null) {
//			// TODO: Discuss with Pond
//			return 0;
//		}
//
//		// This mean there are car ahead and also car that following focused car.
//		else if (followedCar != null && placeHolder != null) {
//			leadCar = placeHolder;
//			return (leadCar.x - leadCar.length) - followedCar.x;
//		}
//
//		return 0;
//
//	}

	public static double GetHarmonicMean(ArrayList<Double> serie) {
		Iterator<Double> serieIt = serie.iterator();
		double sumData = 0;
		while (serieIt.hasNext()) {
			double data = serieIt.next();
			sumData += 1 / data;
		}
		return serie.size() / sumData;
	}
	
	public static double GetAvgSpaceMeanSpeed(Lane lane1, Lane lane2) {
		double sum = lane1.accSpdLd + lane2.accSpdLd;
		double n = lane1.spdLdIndex + lane2.spdLdIndex;
		return n / sum;
	}

//	public static double GetAvgSpaceMeanSpeed(ArrayList<Double> lane1, ArrayList<Double> lane2) {
//		int nPassingCar = lane1.size();
//
//		if (nPassingCar == 0) {
//			return 0;
//		}
//
//		double vAcc = 0;
//		ArrayList<Double> mergedLane = new ArrayList<Double>();
//		Iterator<Double> lane1Iterator = lane1.iterator();
//		while (lane1Iterator.hasNext()) {
//			double v = lane1Iterator.next();
//			vAcc += v;
//			mergedLane.add(v);
//		}
//
//		Iterator<Double> lane2Iterator = lane2.iterator();
//		while (lane2Iterator.hasNext()) {
//			double v = lane2Iterator.next();
//			vAcc += v;
//			mergedLane.add(v);
//		}
//
//		return GetHarmonicMean(mergedLane);
//	}

	private static Car getFirstCarBeforePosition(Lane lane, double position) {
		Iterator<Car> cars = lane.cars.iterator();
		Car result = null;
		while (cars.hasNext()) {
			Car c = cars.next();
			result = c;
			if (c.x <= position) {
				break;
			}
		}

		if (result == null) {
			return null;
		}

		if (result.x - result.length > position) {
			return null;
		}

		return result;
	}

	private static Car getFirstCarAfterPosition(Lane lane, double position) {
		Iterator<Car> cars = lane.cars.iterator();
		Car result = null;
		while (cars.hasNext()) {
			Car c = cars.next();

			if (c.x <= position) {
				break;
			}
			result = c;
		}

		return result;
	}

	private static int getLateralTime(LaneChangeType lcType, boolean isSpaceExceedSpaceCritical) {
		int lanesWidthInFt = 12;
		double speed = 0;
		if (lcType == LaneChangeType.FREE_LC) {
			speed = 2;
		} else if (lcType == LaneChangeType.COOPERATIVE_LC) {
			if (isSpaceExceedSpaceCritical) {
				speed = 2;
			} else {
				speed = 2.5;
			}
		} else if (lcType == LaneChangeType.COMPETITIVE_LC) {
			if (isSpaceExceedSpaceCritical) {
				speed = 2.5;
			} else {
				speed = 3;
			}
		}

		return speed == 0 ? 0 : (int) Math.ceil(lanesWidthInFt / speed);
	}

	private static void logMessage(StringBuilder sb, String message, double time) {
		sb.append(time + " s: " + message);
		sb.append(System.getProperty("line.separator"));
	}

	public static boolean isPlatoonLeading(Lane lane, Car focusedCar, double criticalSpace) {
		int indexCar = lane.cars.indexOf(focusedCar);
		if (indexCar == 0) {
			return true;
		} else {
			Car aheadCar = getAheadCar(focusedCar, lane);

			if (aheadCar == null) {
				return true;
			}

			double space = aheadCar.x - focusedCar.x;
			return space > criticalSpace;
		}
	}

	public static double calculateAccelerator(Lane[] lanes, Car focusedCar, double criticalSpace, boolean avoidCollision) {


		Lane lane = lanes[focusedCar.lane];

		double a, vs, vDiff;

		// is Platoon-leading
		if (focusedCar.isPlatoonLeading) {

			double relaxationTime =  focusedCar.carType == CarType.CAV ? SimulatorConfig.relaxCAV : SimulatorConfig.relaxMDV;

			// First car but lane drop ahead
			double considerLaneChangeAt = focusedCar.carType == CarType.CAV ? SimulatorConfig.laneChangeCAV
					: SimulatorConfig.laneChangeMDV;
			double spaceAhead = lane.dropAt - focusedCar.x;
			if (spaceAhead <= considerLaneChangeAt) {
				vs = getVs(lane.dropAt - focusedCar.x, null, focusedCar);
				vDiff = 0 - focusedCar.v;
				a = ((1 / relaxationTime) * (vs - focusedCar.v)) + (vDiff * focusedCar.sensitivityFactor);
			}
			// First car and no lane drop
			else {
				vs = getVs(100000, null, focusedCar);
				a = (1 / relaxationTime) * (vs - focusedCar.v);
			}
		}

		// Not Leader: Car Following
		else {
			Car aheadCar = getAheadCar(focusedCar, lane);

			CarType considerCarAs = aheadCar.carType == CarType.CAV ? focusedCar.carType : CarType.MDV;
			double relaxationTime =  considerCarAs == CarType.CAV ? SimulatorConfig.relaxCAV : SimulatorConfig.relaxMDV;
			double sensitivityFactor = considerCarAs == CarType.CAV ? SimulatorConfig.cavSensitivity : SimulatorConfig.mdvSensitivity;
			// if focusedCar is changing lane need to consider car from lane that we are
			// changing to
			if (focusedCar.laneToChange != -1) {
				Car aheadCarInChangingLane = getFirstCarAfterPosition(lanes[focusedCar.laneToChange], focusedCar.x);
				if (aheadCarInChangingLane != null && aheadCarInChangingLane.x < aheadCar.x
						&& aheadCarInChangingLane.x > focusedCar.x) {
					aheadCar = aheadCarInChangingLane;
				}
			}

			double differenceX = aheadCar.x - focusedCar.x;
			vs = getVs(differenceX, aheadCar, focusedCar);
			if (differenceX <= criticalSpace) {

				vDiff = aheadCar.v - focusedCar.v;
				a = ((1 / relaxationTime) * (vs - focusedCar.v)) + (vDiff * sensitivityFactor);
			} else {
				a = (1 / relaxationTime) * (vs - focusedCar.v);
			}
		}

		if (a > SimulatorConfig.maxAccelerator) {
			return SimulatorConfig.maxAccelerator;
		} else if (a < SimulatorConfig.minAccelerator) {

			double considerLaneChangeAt = focusedCar.carType == CarType.CAV ? SimulatorConfig.laneChangeCAV
					: SimulatorConfig.laneChangeMDV;
			if (avoidCollision && getSpaceAhead(focusedCar, lane) < considerLaneChangeAt) {
				return a;
			}

			return SimulatorConfig.minAccelerator;
		} else {
			return a;
		}

	}

	public static int getNumCarInRange(Car[] allCars, double detectPosition, double range) {
		int nCar = 0;
		for (int carIndex = 0; carIndex < allCars.length; carIndex++) {
			Car c = allCars[carIndex];
			if (c == null) {
				break;
			}

			if (c.x <= detectPosition) {

				if (c.x < detectPosition - range) {
					break;
				}
				nCar++;
			}
		}
		return nCar;
	}

	public static double getVs(double differenceX, Car aheadCar, Car subjectedCar) {
		
		boolean isAheadCarCAV;
		double aheadCarLength;

		if (aheadCar == null) {
			aheadCarLength = 0;
			isAheadCarCAV = true;
		} else {
			aheadCarLength = aheadCar.length;
			isAheadCarCAV = aheadCar.carType == CarType.CAV;
		}
		
		double spaceAhead = differenceX - aheadCarLength; // this is (s - lc) to calculate V(s)

		boolean isCAV = subjectedCar.carType == CarType.CAV;
		

		// if not following CAV act as MDV
		if (isCAV && isAheadCarCAV) {
			return getCAVConfinedVs(spaceAhead, aheadCar);
		} else {
			return getMDVConfinedVs(spaceAhead, isCAV);
		}
	}

	public static double getMDVConfinedVs(double spaceAhead, boolean isCAV) {
		double v1 = isCAV ? SimulatorConfig.cavVsV1 : SimulatorConfig.mdvVsV1;
		double v2 = isCAV ? SimulatorConfig.cavVsV2 : SimulatorConfig.mdvVsV2;
		double lint = isCAV ? SimulatorConfig.cavVsLint : SimulatorConfig.mdvVsLint;
		double beta = isCAV ? SimulatorConfig.cavVsBeta : SimulatorConfig.mdvVsBeta;

		// V(s) = V1 + [V2 * tanh(((s-lc)/lint) - beta)] 
		return v1 + (v2 * Math.tanh(((spaceAhead) / lint) - beta)); 
	}

	public static double getCAVConfinedVs(double spaceAhead, Car aheadCar) {

		double p = SimulatorConfig.cavVsP;
		double v1 = SimulatorConfig.cavVsV1;
		double v2 = SimulatorConfig.cavVsV2;
		double lint = SimulatorConfig.cavVsLint;
		double beta = SimulatorConfig.cavVsBeta;

		double calculateV;
		if (aheadCar != null && aheadCar.carType == CarType.CAV) {
			calculateV = aheadCar.v;
		} else {
			calculateV = v1;
		}

		// V(s) = PVn + (V2 * tanh[ ( (s-lc) / lint ) - beta ] => for following CAV
		// V(s) = PV1 + (V2 * tanh[ ( (s-lc) / lint ) - beta ] => for other case
		return (p * calculateV) + (v2 * Math.tanh(((spaceAhead) / lint) - beta));
	}

	public static int[] getConstantIndexForConditionalLaneChange(Car inspectCar, Car involvedCar, boolean changeToRight,
			LaneChangeType laneChangeType) {

		StringBuilder sb = new StringBuilder();
		sb.append(inspectCar.isPlatoonLeading ? "PLATOONLEAD" : "CARFOLLOW");
		sb.append("-");

		sb.append(inspectCar.carType.toString());
		sb.append("-");
		sb.append(involvedCar.carType.toString());
		sb.append("-");

		sb.append(changeToRight ? "RIGHT" : "LEFT");
		sb.append("-");

		sb.append(laneChangeType.toString());

		switch (sb.toString()) {
		case "CARFOLLOW-CAV-MDV-LEFT-FREE_LC":
			return new int[] { 9, 9 };
		case "CARFOLLOW-CAV-MDV-LEFT-COOPERATIVE_LC":
			return new int[] { 10, 10 };
		case "CARFOLLOW-CAV-MDV-LEFT-COMPETITIVE_LC":
			return new int[] { 11, 11, 10 };

		case "CARFOLLOW-CAV-CAV-LEFT-FREE_LC":
			return new int[] { 12, 12 };
		case "CARFOLLOW-CAV-CAV-LEFT-COOPERATIVE_LC":
			return new int[] { 13, 13 };

		case "CARFOLLOW-CAV-MDV-RIGHT-FREE_LC":
			return new int[] { 14, 14 };
		case "CARFOLLOW-CAV-MDV-RIGHT-COOPERATIVE_LC":
			return new int[] { 15, 15 };
		case "CARFOLLOW-CAV-MDV-RIGHT-COMPETITIVE_LC":
			return new int[] { 16, 16, 15 };

		case "CARFOLLOW-CAV-CAV-RIGHT-FREE_LC":
			return new int[] { 17, 17 };
		case "CARFOLLOW-CAV-CAV-RIGHT-COOPERATIVE_LC":
			return new int[] { 18, 18 };

		case "PLATOONLEAD-CAV-MDV-LEFT-FREE_LC":
			return new int[] { 19, 19 };
		case "PLATOONLEAD-CAV-MDV-LEFT-COOPERATIVE_LC":
			return new int[] { 20, 20 };
		case "PLATOONLEAD-CAV-MDV-LEFT-COMPETITIVE_LC":
			return new int[] { 21, 21, 20 };

		case "PLATOONLEAD-CAV-CAV-LEFT-FREE_LC":
			return new int[] { 22, 22 };
		case "PLATOONLEAD-CAV-CAV-LEFT-COOPERATIVE_LC":
			return new int[] { 23, 23 };

		case "CARFOLLOW-MDV-CAV-LEFT-FREE_LC":
		case "CARFOLLOW-MDV-MDV-LEFT-FREE_LC":
			return new int[] { 24, 24 };
		case "CARFOLLOW-MDV-CAV-LEFT-COOPERATIVE_LC":
		case "CARFOLLOW-MDV-MDV-LEFT-COOPERATIVE_LC":
			return new int[] { 25, 25 };
		case "CARFOLLOW-MDV-CAV-LEFT-COMPETITIVE_LC":
		case "CARFOLLOW-MDV-MDV-LEFT-COMPETITIVE_LC":
			return new int[] { 26, 26, 25 };

		case "CARFOLLOW-MDV-CAV-RIGHT-FREE_LC":
		case "CARFOLLOW-MDV-MDV-RIGHT-FREE_LC":
			return new int[] { 27, 27 };
		case "CARFOLLOW-MDV-CAV-RIGHT-COOPERATIVE_LC":
		case "CARFOLLOW-MDV-MDV-RIGHT-COOPERATIVE_LC":
			return new int[] { 28, 28 };
		case "CARFOLLOW-MDV-CAV-RIGHT-COMPETITIVE_LC":
		case "CARFOLLOW-MDV-MDV-RIGHT-COMPETITIVE_LC":
			return new int[] { 29, 29, 28 };

		case "PLATOONLEAD-MDV-CAV-LEFT-FREE_LC":
		case "PLATOONLEAD-MDV-MDV-LEFT-FREE_LC":
			return new int[] { 19, 19 };
		case "PLATOONLEAD-MDV-CAV-LEFT-COOPERATIVE_LC":
		case "PLATOONLEAD-MDV-MDV-LEFT-COOPERATIVE_LC":
			return new int[] { 20, 20 };
		case "PLATOONLEAD-MDV-CAV-LEFT-COMPETITIVE_LC":
		case "PLATOONLEAD-MDV-MDV-LEFT-COMPETITIVE_LC":
			return new int[] { 21, 21, 20 };

		default:
			return new int[0];
		}

	}

	public int getSecond(int[] possibilities) {
		return 0;
	}

	public static boolean isAllFinished(Car[] carsArray) {
		boolean isFinished = true;
		for (int i = 0; i < carsArray.length; i++) {
			if (carsArray[i] == null || !carsArray[i].isFinished) {
				isFinished = false;
				break;
			}
		}
		return isFinished;
	}
}
