import java.util.*;
import java.util.regex.Pattern;

public class SimulatorConfig {
	
	public static double[] thresholds;
	public static double[] thresholds_m;
	public static SdData[] gMins;
	public static SdData[] gMinsCongested;
	public static double relaxCAV;
	public static double relaxMDV;
	public static double vMax;
	public static double vInit;
	public static double timeToMonitor;
	public static double roadLength;
	public static double laneDrop;
	public static double laneChangeCAV;
	public static double laneChangeMDV;
	public static double carLength;
	public static double cavSensitivity;
	public static double mdvSensitivity;
	public static double criticalSpace;
	public static int cavCar;
	public static int mdvCar;
	public static double inputCarRate;
	
	public static double gapFree;
	public static double gapMinCAV;
	public static double gapMinMDV;
	

	public static double minAccelerator;
	public static double maxAccelerator;
	public static SdData vD;

	public static double startInspectTime;

	public static double spaceMeanBeforeLD;
	public static double spaceMeanAfterLD;

	public static int outputInterval;
	
	public static double mdvVsV1;
	public static double mdvVsV2;
	public static double mdvVsLint;
	public static double mdvVsBeta;
	public static double cavVsP;
	public static double cavVsV1;
	public static double cavVsV2;
	public static double cavVsLint;
	public static double cavVsBeta;
	
	public static void SetSimulatorConfig (
			double inputCarRate,
			double vMax,
			double vInit,
			double timeToMonitor,
			double roadLength,
			double detectPosition,
			double considerLaneChangeAtCAV,
			double considerLaneChangeAtMDV,
			double carLength,
			double relaxCAV,
			double relaxMDV,
			double cavConst,
			double mdvConst,
			double gapFree,
			double gapMinCAV,
			double gapMinMDV,
			double criticalSpace,
			int nCavCar,
			int nMdvCar,
			double[] thresholds,
			double[] thresholds_m,
			SdData[] gMins,
			SdData[] gMinsCongested,
			double minA,
			double maxA,
			SdData vD,
			double tAvgDensity,
			double spaceMeanBeforeLD,
			double spaceMeanAfterLD,
			int outputInterval,
			double mdvVsV1,
			double mdvVsV2,
			double mdvVsLint,
			double mdvVsBeta,
			double cavVsP,
			double cavVsV1,
			double cavVsV2,
			double cavVsLint,
			double cavVsBeta) {

		SimulatorConfig.inputCarRate = inputCarRate;
		SimulatorConfig.thresholds = thresholds;
		SimulatorConfig.thresholds_m = thresholds_m;
		SimulatorConfig.gMins = gMins;
		SimulatorConfig.gMinsCongested = gMinsCongested;

		SimulatorConfig.gapFree = gapFree;
		SimulatorConfig.gapMinCAV = gapMinCAV;
		SimulatorConfig.gapMinMDV = gapMinMDV;
		SimulatorConfig.relaxCAV = relaxCAV;
		SimulatorConfig.relaxMDV = relaxMDV;
		SimulatorConfig.vMax = vMax;
		SimulatorConfig.vInit = vInit;
		SimulatorConfig.timeToMonitor = timeToMonitor;
		SimulatorConfig.roadLength = roadLength;
		SimulatorConfig.laneDrop = detectPosition;
		SimulatorConfig.laneChangeCAV = considerLaneChangeAtCAV;
		SimulatorConfig.laneChangeMDV = considerLaneChangeAtMDV;
		SimulatorConfig.carLength = carLength;
		SimulatorConfig.cavSensitivity = cavConst;
		SimulatorConfig.mdvSensitivity = mdvConst;
		SimulatorConfig.criticalSpace = criticalSpace;

		SimulatorConfig.cavCar = nCavCar;
		SimulatorConfig.mdvCar = nMdvCar;

		SimulatorConfig.minAccelerator = minA;
		SimulatorConfig.maxAccelerator = maxA;
		

		SimulatorConfig.startInspectTime = tAvgDensity;
		
		SimulatorConfig.vD = vD;
		
		SimulatorConfig.outputInterval = outputInterval;
		SimulatorConfig.spaceMeanBeforeLD = spaceMeanBeforeLD;
		SimulatorConfig.spaceMeanAfterLD = spaceMeanAfterLD;
		

		SimulatorConfig.mdvVsV1 = mdvVsV1;
		SimulatorConfig.mdvVsV2 = mdvVsV2;
		SimulatorConfig.mdvVsLint = mdvVsLint;
		SimulatorConfig.mdvVsBeta = mdvVsBeta;
		
		SimulatorConfig.cavVsP = cavVsP;
		SimulatorConfig.cavVsV1 = cavVsV1;
		SimulatorConfig.cavVsV2 = cavVsV2;
		SimulatorConfig.cavVsLint = cavVsLint;
		SimulatorConfig.cavVsBeta = cavVsBeta;
	}

	public static double getThreshold(int i) {
		double result = SimulatorConfig.thresholds[i - 1];
		return result;
	}
	public static double getThresholdM(int i) {
		double result = SimulatorConfig.thresholds_m[i - 1];
		return result;
	}
	
	public static double getGMins(int i) {
		double result = SimulatorConfig.gMins[i - 1].getValue();
		return result;
	}
	public static double getGMins_Congested(int i) {
		SdData result = SimulatorConfig.gMinsCongested[i - 1];
		
		if (result == null) {
			return Double.NaN;
		}
		
		return result.getValue();
	}
		
}

class SdData {
	double mean;
	double sd;
	
	public SdData (String formatString) {
		if (formatString == null) {
			formatString = "0";
		}
		String[] splitted = formatString.split(Pattern.quote("|"));
		this.mean = Double.parseDouble(splitted[0]);
		if (splitted.length > 1) {
			this.sd = Double.parseDouble(splitted[1]);
		}
		
	}
	
	public SdData (double mean, double sd) {
		this.mean = mean;
		this.sd = sd;
	}
	
	public double getValue () {
		Random randomno = new Random();
		return mean + (randomno.nextGaussian() * sd);
	}
}